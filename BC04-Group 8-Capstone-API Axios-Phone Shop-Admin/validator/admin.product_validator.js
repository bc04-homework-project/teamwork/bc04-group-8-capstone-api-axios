export let validation = {
  checkEmpty: function (value, idError, subject) {
    if (value.length == 0) {
      document.getElementById(
        idError
      ).innerText = `${subject} không được để trống`;
      return false;
    }
    document.getElementById(idError).innerText = "";
    return true;
  },
  checkDuplicate: function (value, productList, idError, message) {
    var index = productList.findIndex(function (product) {
      return value == product.id;
    });
    if (index == -1) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = message;
    return false;
  },
  checkNumber: function (value, idError, subject) {
    const numbers = /^\d+$/;
    if (value.match(numbers)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = `${subject} phải là số`;
    return false;
  },
};
